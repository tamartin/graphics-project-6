#ifndef CURVE_H
#define CURVE_H

#include "matrix4.h"
#include "cvec.h"

class curve {
  public:
    Matrix4 cp1;
    Matrix4 cp2;
    Matrix4 cp3;
    Matrix4 cp4;
    //Initialize Variables
    curve(Matrix4 control1, Matrix4 control2, Matrix4 control3, Matrix4 control4);

    // Computes a Cvec3 for the point where the given Matrix4 is located
    Cvec3 getPoint(Matrix4 mat);

    //Find the right spot on the curve depending on the clock time
    Matrix4 getLocation(float clock);

    //Find the right orientation depending on the clock time
    Matrix4 getOrientation(float clock);

};

#endif
