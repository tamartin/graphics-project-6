#include <iostream>
#include "curve.h"
using namespace std;

curve::curve(Matrix4 control1, Matrix4 control2, Matrix4 control3, Matrix4 control4)
{
  this->cp1 = control1;
  this->cp2 = control2;
  this->cp3 = control3;
  this->cp4 = control4;
}

Cvec3 curve::getPoint(Matrix4 mat){
  Cvec3 point = Cvec3(mat[3], mat[7], mat[11]);
  return point;
}

Matrix4 curve::getLocation(float clock){
  Matrix4 loc = Matrix4::makeTranslation(getPoint(cp1) * pow(1-clock,3) + getPoint(cp2) * 3 * clock * pow(1-clock,2) + getPoint(cp3) * 3 * pow(clock,2) *(1-clock) + getPoint(cp4) * pow(clock,3));
  return loc;
}

Matrix4 curve::getOrientation(float clock){
  Cvec3 currentloc = getPoint(getLocation(clock));
  Cvec3 newloc = getPoint(getLocation(clock + 0.01));
  Cvec3 difference = currentloc - newloc;
  Cvec3 z = normalize(difference);
  Cvec3 x = normalize(cross(Cvec3(0,1,0), z));
  Cvec3 y = cross(z, x);
  Matrix4 orientation = Matrix4();
  orientation(0,0) = x[0];
  orientation(1,0) = x[1];
  orientation(2,0) = x[2];
  orientation(0,1) = y[0];
  orientation(1,1) = y[1];
  orientation(2,1) = y[2];
  orientation(0,2) = z[0];
  orientation(1,2) = z[1];
  orientation(2,2) = z[2];
  return orientation; 
}
